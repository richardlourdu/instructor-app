import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Emptyassignments from './components/Emptyassignments.vue';
import { publicPath } from '../vue.config.js';

Vue.config.productionTip = false

Vue.use(VueRouter)
console.log(publicPath);
const router = new VueRouter({
  mode: 'history',
  routes: [
   { path:  publicPath, component: Emptyassignments }
  ]
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
