module.exports = {
	publicPath: '/instructor-app/dist/',
	outputDir: 'dist',
    devServer: {
      disableHostCheck: true
   }
  }